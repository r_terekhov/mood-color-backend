FROM anapsix/alpine-java:8_jdk

#setup UTC timezone
RUN apk update && \
    apk add tzdata && \
    cp /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
    echo "Etc/UTC" >  /etc/timezone && \
    apk del tzdata

WORKDIR /app
ADD ./target/mood-color-backend.jar .

CMD ["java", "-Dspring.profiles.active=prod", "-Djava.net.preferIPv4Stack=true", "-jar", "mood-color-backend.jar"]

EXPOSE 8080