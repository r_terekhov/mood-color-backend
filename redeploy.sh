#!/bin/bash

DEFAULT_TAG=latest
read -p "Image TAG($DEFAULT_TAG): " TAG
TAG=${TAG:-$DEFAULT_TAG}

git pull
mvn clean install -DskipTests
docker build -t registry.gitlab.com/source-team/mood-color-backend:$TAG .
docker push registry.gitlab.com/source-team/mood-color-backend:$TAG