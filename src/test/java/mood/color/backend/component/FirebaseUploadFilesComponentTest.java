/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.component;

import mood.color.backend.config.FirebaseConfig;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@ActiveProfiles("local")
@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {FirebaseConfig.class, TestUploadFileComponent.class}
)
@EnableConfigurationProperties
class FirebaseUploadFilesComponentTest {


    @Autowired
    @Qualifier("testUploadFileComponent")
    UploadFileComponent uploadFileComponent;

    @Test
    void uploadFileToFirebase() throws IOException {
        File file = new File("/home/roman/Work/mood-color-backend/src/test/resourses/tinder.png");
        FileInputStream input = new FileInputStream(file);
        MultipartFile multipartFile = new MockMultipartFile("file",
                file.getName(), "text/plain", IOUtils.toByteArray(input));
        String url = uploadFileComponent.uploadFileToFirebase(multipartFile);
        System.out.println(url);
    }
}