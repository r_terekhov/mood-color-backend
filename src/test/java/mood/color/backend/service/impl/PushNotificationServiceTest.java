/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.service.impl;

import mood.color.backend.config.FirebaseConfig;
import mood.color.backend.model.PushNotificationRequest;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(
        classes = {FirebaseConfig.class, FCMService.class, PushNotificationService.class}
)
@EnableConfigurationProperties
@Ignore
class PushNotificationServiceTest {

    @Autowired
    PushNotificationService pushNotificationService;

    @Test
    void sendPushNotificationToToken() {
        pushNotificationService.sendPushNotificationToToken(
                new PushNotificationRequest("Hey you!", "Watch out!", "", "eW36aWZjGo4:APA91bGvqYh55bUIWQ7Cu8-SfuA878mXVwzK8T7FfdiB1ip_pKj2BER1fIYirPnsp0EESKDxS7kLGerG6V2E9ZMFrNQlSlt1W2XgIYgGf8xYqdgNKVm36gc-XgVyIwJ66Imt4LExBVDL"));
    }
}