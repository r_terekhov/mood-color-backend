/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.service.impl;

import mood.color.backend.Application;
import mood.color.backend.domain.Event;
import mood.color.backend.dto.ContactResponse;
import mood.color.backend.dto.CreateEventRequestDto;
import mood.color.backend.model.Contact;
import mood.color.backend.model.ContactType;
import mood.color.backend.service.EventService;
import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("local")
@DataMongoTest
@ComponentScan(basePackages = {"mood.color.backend"})
@EnableMongoRepositories(basePackages = {"mood.color.backend.repository"})
@ContextConfiguration(classes = Application.class)
@RunWith(SpringRunner.class)
@Ignore
class EventServiceImplTest {

    @Autowired
    EventService eventService;

    @Test
    void createEvent() throws IOException {

        File file = new File("/home/roman/Work/mood-color-backend/src/test/resourses/tinder.png");
        FileInputStream input = new FileInputStream(file);
        MultipartFile multipartFile = new MockMultipartFile("file",
                file.getName(), "text/plain", IOUtils.toByteArray(input));


        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact(ContactType.PHONE, "89998887766"));
        contacts.add(new Contact(ContactType.VK, "vk.com"));


        List<ContactResponse> contactDto = new ArrayList<>();

        contacts.forEach(contact -> contactDto.add(new ContactResponse(contact.getType().getType(), contact.getValue())));

        CreateEventRequestDto createEventRequestDto = new CreateEventRequestDto(
                "id",
                "name",
                multipartFile,
                "description",
                "19.03.1998 13:00",
                "трц Эдем",
                50,
                50,
                contactDto,
                "#ff0000");

        Event event = eventService.createEvent(createEventRequestDto);
        System.out.println(event);

    }


    @Test
    void getEvents() {

        List<Event> firstPage = eventService.getEventsByGeo(50, 50, 100000, 0);
        assertEquals(10, firstPage.size());

        Pageable page2 = PageRequest.of(0, 20);
        List<Event> secondPage = eventService.getEventsByGeo(50, 50, 100000, 1);
        assertEquals(20, secondPage.size());


        List<Event> thirdPage = eventService.getEventsByGeo(50, 50, 100000, 4);
        assertEquals(0, thirdPage.size());


    }

}