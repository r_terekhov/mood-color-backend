/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UtilsTest {

    @Test
    void convertDateToEpochSecond() {
        String dateInString = "19.03.1998 11:00";
        Long dateToEpochSecond = Utils.convertDateToEpochSecond(dateInString);

        String epochSecondToString = Utils.covertEpochSecondToString(dateToEpochSecond);
        assertEquals(dateInString, epochSecondToString);
    }
}