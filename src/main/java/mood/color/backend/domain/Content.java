/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.domain;

import javax.validation.constraints.NotBlank;
import java.time.Instant;

public class Content extends ExpiredDocument {

    @NotBlank
    private String contentType;

    @NotBlank
    private String pathToFile;


    protected Content(Instant expireAt, @NotBlank String contentType, @NotBlank String pathToFile) {
        super(expireAt);
        this.contentType = contentType;
        this.pathToFile = pathToFile;
    }
}
