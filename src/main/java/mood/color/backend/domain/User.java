/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * uniqueId - id инстанса приложения, или токен при авторизации через соцсеть,
 * по которому мы можем идентифицировать пользователя
 * <p>
 * gender :
 * 0 - woman
 * 1 - man
 * 2 - not setup
 */
@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User extends Document {
    @NotBlank
    @Indexed(unique = true)
    private String pushToken;

    @NotBlank
    @Indexed(unique = true)
    private String uniqueId;

    @Nullable
    @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
    private GeoJsonPoint location;

    @NotNull
    private Integer gender = 2;

    @NotNull
    private String nick;

    @NotBlank
    private String zoneOffset;

    @NotBlank
    private String locale;

    public User(@NotBlank String pushToken, @NotBlank String uniqueId, @NotBlank String zoneOffset, @NotBlank String locale) {
        this.pushToken = pushToken;
        this.uniqueId = uniqueId;
        this.zoneOffset = zoneOffset;
        this.locale = locale;
    }
}