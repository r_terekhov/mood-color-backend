/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = false)
@Data
public class Graph extends Document {

    @NotBlank
    @Getter
    private String creatorId;

    @NotNull
    @Getter
    @Setter
    private Integer amountOfLabelsInGraph;

    @NotNull
    @Getter
    @Setter
    @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
    private GeoJsonPoint location;


    public Graph(@NotBlank String creatorId, @NotNull GeoJsonPoint location) {
        this.creatorId = creatorId;
        this.location = location;
        this.amountOfLabelsInGraph = 0;
    }
}
