/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.domain;

import lombok.*;
import mood.color.backend.model.Contact;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;


@EqualsAndHashCode(callSuper = false)
@Data
@ToString
public class Event extends ExpiredDocument {

    @NotBlank
    @Getter
    private String creatorId;

    @NotBlank
    @Getter
    private String name;

    @NotBlank
    @Getter
    private String pathToAvatar;

    @NotBlank
    @Getter
    private String description;

    @NotBlank
    @Getter
    private Instant dateAndTime;

    @Getter
    @NotBlank
    private String humanReadableGeo;


    @NotNull
    @Getter
    @Setter
    @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
    private GeoJsonPoint location;

    @Getter
    @Nullable
    private List<Contact> contacts;


    @NotBlank
    private String backgroundColor;


    public Event(Instant expireAt, @NotBlank String creatorId, @NotBlank String name, @NotBlank String pathToAvatar, @NotBlank String description, @NotBlank Instant dateAndTime, @NotBlank String humanReadableGeo, @NotNull GeoJsonPoint location, @Nullable List<Contact> contacts, @NotBlank String backgroundColor) {
        super(expireAt);
        this.creatorId = creatorId;
        this.name = name;
        this.pathToAvatar = pathToAvatar;
        this.description = description;
        this.dateAndTime = dateAndTime;
        this.humanReadableGeo = humanReadableGeo;
        this.location = location;
        this.contacts = contacts;
        this.backgroundColor = backgroundColor;
    }
}
