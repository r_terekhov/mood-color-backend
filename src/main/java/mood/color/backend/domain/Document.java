/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.Indexed;

import java.time.Instant;


public abstract class Document {
    @Transient
    private static final Double CURRENT_SCHEMA_VERSION = 2.0;

    @Id
    @Getter
    @Indexed(unique = true)
    private String id;

    @Getter
    @CreatedDate
    @Setter
    private Instant created;

    @Version
    @Indexed
    @Getter
    private Integer version;

    @Getter
    @Indexed
    private Double schemaVersion = CURRENT_SCHEMA_VERSION;


    public Document() {
        this.created = Instant.now();
    }

    public Document(String id) {
        this.id = id;
        this.created = Instant.now();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Document document = (Document) o;

        return id.equals(document.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
