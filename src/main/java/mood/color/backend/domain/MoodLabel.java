/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import mood.color.backend.model.Action;
import mood.color.backend.model.Mood;
import mood.color.backend.model.Status;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = false)

public class MoodLabel extends ExpiredDocument {

    @NotBlank
    @Getter
    private String creatorId;

    @NotNull
    @Getter
    private Integer genderOfCreator;

    @NotBlank
    @Getter
    @Indexed
    private String graphId;

    @NotNull
    @Getter
    @Setter
    @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
    private GeoJsonPoint location;


    @NotNull
    @Getter
    @Indexed
    private Mood mood;

    @Nullable
    @Getter
    private Action action;

    @NotNull
    @Getter
    private Status status;


    @Nullable
    @Getter
    private List<Content> contents = new ArrayList<>();

    @NotNull
    @Getter
    private Boolean longLive;


    public MoodLabel(Instant expireAt, @NotBlank String creatorId, Integer genderOfCreator, @NotBlank String graphId, @NotNull GeoJsonPoint location, @NotNull Mood mood, @Nullable Action action, Boolean longLive, @NotNull Status status) {
        super(expireAt);
        this.creatorId = creatorId;
        this.genderOfCreator = genderOfCreator;
        this.graphId = graphId;
        this.location = location;
        this.mood = mood;
        this.action = action;
        this.longLive = longLive;
        this.status = status;
    }
}
