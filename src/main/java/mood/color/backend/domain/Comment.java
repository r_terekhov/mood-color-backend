/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

public class Comment extends ExpiredDocument {

    @NotBlank
    @Getter
    private String creatorId;

    @NotBlank
    @Getter
    private String authorNick;

    @NotNull
    @Getter
    private Integer authorGender;

    @NotBlank
    @Getter
    @Indexed(unique = true)
    private String moodLabelId;


    @NotBlank
    @Setter
    @Getter
    private String body;


    public Comment(Instant expireAt, @NotBlank String creatorId, @NotBlank String authorNick, Integer authorGender, @NotBlank String moodLabelId, @NotBlank String body) {
        super(expireAt);
        this.creatorId = creatorId;
        this.authorNick = authorNick;
        this.authorGender = authorGender;
        this.moodLabelId = moodLabelId;
        this.body = body;
    }
}
