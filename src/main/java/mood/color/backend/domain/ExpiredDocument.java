/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.lang.NonNull;

import java.time.Instant;

@EqualsAndHashCode(callSuper = false)
@Data
public abstract class ExpiredDocument extends Document {

    private @NonNull
    Instant expireAt;

    protected ExpiredDocument(@NonNull Instant expireAt) {
        super();
        this.expireAt = expireAt;
    }
}
