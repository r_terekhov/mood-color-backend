/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
public enum ContactType {
    VK("vk"),
    INSTAGRAM("instagram"),
    SITE("site"),
    PHONE("phone"),
    EMAIL("email");


    @Getter
    private String type;


    public static ContactType fromString(String contactType) {
        switch (contactType) {
            case "vk":
                return VK;
            case "instagram":
                return INSTAGRAM;
            case "site":
                return SITE;
            case "phone":
                return PHONE;
            case "email":
                return EMAIL;

            default: {
                throw new IllegalArgumentException(String.format("Available contactTypes are %s, found %s", Arrays.toString(ContactType.values()), contactType));
            }
        }
    }

    public static String fromContactType(ContactType contactType) {
        return contactType.getType();
    }


}
