/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.model;

public enum Action {
    NOACTION,
    VALUE;

    public static Action fromInteger(Integer action) {

        if (action == null) {
            return Action.NOACTION;
        }

        switch (action) {
            case 0: {
                return Action.NOACTION;
            }
            case 1: {
                return Action.VALUE;
            }
            //todo
            default: {
                throw new IllegalArgumentException(String.format("Action must be from 1 to 5 , found %s", action));
            }
        }
    }


    public static int fromAction(Action action) {
        if (action == null) {
            return 0;
        }

        return action.ordinal();
    }
}
