/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
public class BaseApiResponse<T> {
    private T response;
    private String error;

    private BaseApiResponse(T response, String error) {
        this.response = response;
        this.error = error;
    }

    public static <T> BaseApiResponse<T> response(T response) {
        return new BaseApiResponse<>(response, null);
    }

    public static <T> BaseApiResponse<T> error(String error) {
        return new BaseApiResponse<>(null, error);
    }
}
