/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Mood {
    SUPER_DISLIKE(1),
    DISLIKE(2),
    NEUTRAL(3),
    LIKE(4),
    SUPER_LIKE(5),
    NO_MOOD(6);

    public static Mood fromInteger(Integer mood) {
        if (mood == null) {
            return NO_MOOD;
        }

        switch (mood) {
            case 1:
                return SUPER_DISLIKE;
            case 2:
                return DISLIKE;
            case 3:
                return NEUTRAL;
            case 4:
                return LIKE;
            case 5:
                return SUPER_LIKE;

            case 6:
                return NO_MOOD;
            default: {
                throw new IllegalArgumentException(String.format("Mood must be from 1 to 6 , found %s", mood));
            }
        }
    }

    public static int fromMood(Mood mood) {
        return mood.ordinal() + 1;
    }


    @Getter
    private Integer mood;
}
