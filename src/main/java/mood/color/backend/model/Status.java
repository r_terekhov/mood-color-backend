/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Status {
    DEAD(0),
    ALIVE(1);

    public static Status fromInteger(Integer status) {
        if (status == null) {
            return DEAD;
        }

        switch (status) {
            case 0:
                return DEAD;
            case 1:
                return ALIVE;
            default: {
                throw new IllegalArgumentException(String.format("LabelStatus must be from 0 to 1 , found %s", status));
            }
        }
    }

    public static int fromStatus(Status status) {
        return status.ordinal();
    }


    @Getter
    private Integer status;
}
