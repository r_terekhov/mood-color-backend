/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.model;

public enum ContentType {
    PHOTO,
    VIDEO
}
