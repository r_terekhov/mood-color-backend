/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@Profile("local")
@Component
public class TestUploadFileComponent implements UploadFileComponent {

    private final Logger logger = LoggerFactory.getLogger(TestUploadFileComponent.class.getName());


    @Override
    @Profile("local")
    public String uploadFileToFirebase(MultipartFile multipartFile) throws FileNotFoundException {
        File file = convertMultiPartFileToFile(multipartFile);
        InputStream testFile = new FileInputStream(file);
        String blobString = "avatars/" + file.getName();
        logger.info("test log on success saving file");
        return blobString;
    }
}
