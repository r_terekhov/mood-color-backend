/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.component;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import static java.lang.String.format;

@Profile("prod")
@Component
public class FirebaseUploadFilesComponent implements UploadFileComponent {

    private final Bucket bucket;


    public FirebaseUploadFilesComponent(Bucket bucket) {
        this.bucket = bucket;
    }

    @Profile("prod")
    @Override
    public String uploadFileToFirebase(MultipartFile multipartFile) throws FileNotFoundException {
        File file = convertMultiPartFileToFile(multipartFile);
        InputStream testFile = new FileInputStream(file);
        String blobString = "avatars/" + file.getName();
        Blob blob = bucket.create(blobString, testFile, Bucket.BlobWriteOption.userProject("mood-color"));
        return format("https://storage.googleapis.com/mood-color.appspot.com/avatars/%s", file.getName());
    }


}
