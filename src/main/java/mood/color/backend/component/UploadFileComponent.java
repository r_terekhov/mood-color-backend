/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.component;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import static java.lang.String.format;

public interface UploadFileComponent {
    Logger logger = LoggerFactory.getLogger(UploadFileComponent.class.getName());

    String uploadFileToFirebase(MultipartFile multipartFile) throws FileNotFoundException;

    default File convertMultiPartFileToFile(MultipartFile file) {
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        File convertedFile = new File(format("%s.%s", UUID.randomUUID(), extension));
        try (FileOutputStream fos = new FileOutputStream(convertedFile)) {
            fos.write(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(format("error on converting multipart file to file %s", e.getMessage()));
        }
        return convertedFile;
    }
}
