/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.config;

import org.springframework.core.convert.converter.Converter;

import java.time.Instant;
import java.util.Date;

public class InstantTimeReadConverter implements Converter<Date, Instant> {
    @Override
    public Instant convert(Date date) {
        return date.toInstant();
    }
}