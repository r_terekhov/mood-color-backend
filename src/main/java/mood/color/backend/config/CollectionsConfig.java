/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.config;

import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeospatialIndex;
import org.springframework.data.mongodb.core.index.Index;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

@Configuration
@DependsOn("mongoTemplate")
public class CollectionsConfig {

    private final MongoTemplate mongoTemplate;

    public CollectionsConfig(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @PostConstruct
    public void initIndexes() {
        mongoTemplate.indexOps("user") // collection name string or .class
                .ensureIndex(
                        new Index("uniqueId", Sort.Direction.ASC).unique());

        mongoTemplate.indexOps("user") // collection name string or .class
                .ensureIndex(
                        new GeospatialIndex("location.coordinates").typed(GeoSpatialIndexType.GEO_2DSPHERE));

        mongoTemplate.indexOps("moodLabel") // collection name string or .class
                .ensureIndex(
                        new GeospatialIndex("location.coordinates").typed(GeoSpatialIndexType.GEO_2DSPHERE));

        mongoTemplate.indexOps("event") // collection name string or .class
                .ensureIndex(
                        new GeospatialIndex("location.coordinates").typed(GeoSpatialIndexType.GEO_2DSPHERE));

        mongoTemplate.indexOps("moodLabel") // collection name string or .class
                .ensureIndex(
                        new Index("graphId", Sort.Direction.ASC));


        mongoTemplate.indexOps("graph") // collection name string or .class
                .ensureIndex(
                        new GeospatialIndex("location.coordinates").typed(GeoSpatialIndexType.GEO_2DSPHERE));


        //ttl
        mongoTemplate.getCollection("moodLabel").createIndex(Indexes.ascending("expireAt"),
                new IndexOptions().expireAfter(0L, TimeUnit.SECONDS));

        mongoTemplate.getCollection("graph").createIndex(Indexes.ascending("expireAt"),
                new IndexOptions().expireAfter(0L, TimeUnit.SECONDS));

        mongoTemplate.getCollection("comment").createIndex(Indexes.ascending("expireAt"),
                new IndexOptions().expireAfter(0L, TimeUnit.SECONDS));

        mongoTemplate.getCollection("content").createIndex(Indexes.ascending("expireAt"),
                new IndexOptions().expireAfter(0L, TimeUnit.SECONDS));

        mongoTemplate.getCollection("event").createIndex(Indexes.ascending("expireAt"),
                new IndexOptions().expireAfter(0L, TimeUnit.SECONDS));


    }
}