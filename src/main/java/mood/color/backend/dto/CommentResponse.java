/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.dto;

import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Value
public class CommentResponse extends BaseResponseDto {

    @NotBlank
    private final String creatorId;

    @NotBlank
    private final String authorNick;

    @NotNull
    private final Integer authorGender;

    @NotBlank
    private final String moodLabelId;


    @NotBlank
    private final String body;

    public CommentResponse(String id, long created, @NotBlank String creatorId, @NotBlank String authorNick, @NotNull Integer authorGender, @NotBlank String moodLabelId, @NotBlank String body) {
        super(id, created);
        this.creatorId = creatorId;
        this.authorNick = authorNick;
        this.authorGender = authorGender;
        this.moodLabelId = moodLabelId;
        this.body = body;
    }
}
