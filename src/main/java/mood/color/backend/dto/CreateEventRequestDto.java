/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.dto;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.lang.Nullable;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@RequiredArgsConstructor
@Value
public class CreateEventRequestDto {

    @NotBlank
    private final String creatorId;

    @NotBlank
    private final String name;

    @NotNull
    private final MultipartFile avatar;

    @NotBlank
    private final String description;

    @NotBlank
    /*ex : dd.MM.yyyy hh:mm   - in utc*/
    private final String dateAndTime;

    @NotBlank
    private String humanReadableGeo;

    @NotNull
    private double lat;

    @NotNull
    private double lng;

    @Nullable
    private List<ContactResponse> contacts;

    @NotBlank
    private String backgroundColor;



}
