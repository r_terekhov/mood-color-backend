/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.dto;

import lombok.EqualsAndHashCode;
import lombok.Value;
import mood.color.backend.domain.Content;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Value
public class LabelResponse extends BaseResponseDto {

    @NotBlank
    private final String creatorId;

    @NotNull
    private final Integer genderOfCreator;

    @NotBlank
    private final String graphId;

    @NotNull
    private final Double lat;

    @NotNull
    private final Double lng;


    @NotNull
    private final Integer mood;


    @NotNull
    private final Integer action;


    @Nullable
    private final List<Content> contents;

    @Nullable
    private final Boolean longLive;

    @NotNull
    private final Integer status;

    @NotNull
    private final Long expireAt;


    public LabelResponse(String id, long created, @NotBlank String creatorId, @NotNull Integer genderOfCreator, @NotBlank String graphId, @NotNull Double lat, @NotNull Double lng, @NotNull Integer mood, @NotNull Integer action, @Nullable List<Content> contents, @Nullable Boolean longLive, Integer status, Long expireAt) {
        super(id, created);
        this.creatorId = creatorId;
        this.genderOfCreator = genderOfCreator;
        this.graphId = graphId;
        this.lat = lat;
        this.lng = lng;
        this.mood = mood;
        this.action = action;
        this.contents = contents;
        this.longLive = longLive;
        this.status = status;
        this.expireAt = expireAt;
    }
}
