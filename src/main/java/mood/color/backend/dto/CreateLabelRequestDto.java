/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.dto;

import lombok.Value;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Value
public class CreateLabelRequestDto {
    @NotBlank
    final String creatorId;
    @NotNull
    final Integer genderOfCreator;
    @NotNull
    final Double lat;
    @NotNull
    final Double lng;
    @NotNull
    final Integer mood;
    @Nullable
    final Integer action;
    @Nullable
    final String message;
    @Nullable
    final Boolean longLive;

}
