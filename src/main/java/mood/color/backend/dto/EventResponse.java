/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.dto;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@EqualsAndHashCode(callSuper = false)
@Value
public class EventResponse extends BaseResponseDto {


    @NotBlank
    private String creatorId;

    @NotBlank
    private String name;

    @NotBlank
    private String pathToAvatar;

    @NotBlank
    private String description;

    @NotBlank
    private long dateAndTime;

    @NotBlank
    private String humanReadableGeo;


    @NotNull
    private final Double lat;

    @NotNull
    private final Double lng;

    @Nullable
    private List<ContactResponse> contacts;


    @NotNull
    private final Long expireAt;

    @NotBlank
    private String backgroundColor;


    public EventResponse(String id, long created, @NotBlank String creatorId, @NotBlank String name, @NotBlank String pathToAvatar, @NotBlank String description, @NotBlank long dateAndTime, @NotBlank String humanReadableGeo, @NotNull Double lat, @NotNull Double lng, @Nullable List<ContactResponse> contacts, Long expireAt, @NotBlank String backgroundColor) {
        super(id, created);
        this.creatorId = creatorId;
        this.name = name;
        this.pathToAvatar = pathToAvatar;
        this.description = description;
        this.dateAndTime = dateAndTime;
        this.humanReadableGeo = humanReadableGeo;
        this.lat = lat;
        this.lng = lng;
        this.contacts = contacts;
        this.expireAt = expireAt;
        this.backgroundColor = backgroundColor;
    }
}
