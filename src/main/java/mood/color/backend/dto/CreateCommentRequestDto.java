/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.dto;

import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Value
public class CreateCommentRequestDto {
    private final @NotBlank String moodLabelId;
    private final @NotBlank String creatorId;
    private final @NotBlank String authorNick;
    private final @NotNull Integer authorGender;
    private final @NotBlank String body;


}
