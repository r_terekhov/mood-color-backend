/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.dto;

import lombok.RequiredArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
@RequiredArgsConstructor
public class UserRegistrationRequestDto {
    @NotBlank
    private final String pushToken;

    @NotBlank
    private final String uniqueId;


    @NotBlank
    private final String zoneOffset;

    @NotBlank
    private final String locale;

}
