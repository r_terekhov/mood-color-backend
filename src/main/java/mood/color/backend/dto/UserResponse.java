/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Setter;
import mood.color.backend.domain.User;
import mood.color.backend.utils.Utils;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@Builder
@Data
public class UserResponse {

    @NotBlank
    private final String id;

    @NotBlank
    private final String pushToken;

    @NotBlank
    private final String uniqueId;

    @Nullable
    @Setter
    private Double lat;

    @Nullable
    @Setter
    private Double lng;

    @Nullable
    private final Integer gender;

    @NotBlank
    private final String nick;

    @NotBlank
    private final String zoneOffset;

    @NotBlank
    private final String locale;

    @NotNull
    private final long created;


    public static UserResponse of(User user) {
        UserResponse userResponse = UserResponse.builder()
                .pushToken(user.getPushToken())
                .id(user.getId())
                .uniqueId(user.getUniqueId())
                .gender(user.getGender())
                .nick(user.getNick())
                .zoneOffset(user.getZoneOffset())
                .locale(user.getLocale())
                .created(user.getCreated().getEpochSecond()).build();


        List<Double> locationToLatLng = Utils.convertLocationToLatLng(user.getLocation());

        userResponse.setLat(locationToLatLng.get(0));
        userResponse.setLat(locationToLatLng.get(1));


        return userResponse;
    }
}
