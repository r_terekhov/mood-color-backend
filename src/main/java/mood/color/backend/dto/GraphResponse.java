/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.dto;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import mood.color.backend.domain.Graph;
import mood.color.backend.utils.Utils;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Value
@RequiredArgsConstructor
public class GraphResponse {

    //todo extends baseResponse

    @NotBlank
    private final String id;

    @NotBlank
    private final String creatorId;

    @NotNull
    private final Integer amountOfLabelsInGraph;

    @NotNull
    private final Double lat;

    @NotNull
    private final Double lng;

    @NotNull
    private final long created;


    public static GraphResponse of(Graph graph) {
        List<Double> locationToLatLng = Utils.convertLocationToLatLng(graph.getLocation());
        return new GraphResponse(
                graph.getId(),
                graph.getCreatorId(),
                graph.getAmountOfLabelsInGraph(),
                locationToLatLng.get(0),
                locationToLatLng.get(1),
                graph.getCreated().toEpochMilli());
    }

}
