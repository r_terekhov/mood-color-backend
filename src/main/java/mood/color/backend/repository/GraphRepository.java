/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.repository;

import mood.color.backend.domain.Graph;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GraphRepository extends MongoRepository<Graph, String>, CustomGraphRepository {
/*
    GeoResults<Graph> findAllByLocationNear(Point point, Distance distance);*/
}
