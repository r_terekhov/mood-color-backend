/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.repository;

import mood.color.backend.domain.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CommentRepository extends MongoRepository<Comment, String> {

    Page<Comment> findAllByMoodLabelIdInAndIdNotIn(Set<String> labelsIds, Set<String> excludeCommentsIds, Pageable pageable);
}
