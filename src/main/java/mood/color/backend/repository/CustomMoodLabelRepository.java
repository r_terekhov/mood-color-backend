/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.repository;

import mood.color.backend.domain.MoodLabel;

import java.time.Instant;
import java.util.List;

public interface CustomMoodLabelRepository {
    /*https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#reference*/
    List<MoodLabel> getMoodLabelsByPointAndDistance(Double lat, Double lng, Double distanceInMetres);

    List<MoodLabel> findMoodLabelsByCreatorIdAndExpireAtBeforeOrderByExpireAtDescAndByPointAndDistance(String creatorId, Instant expireBefore, Double lat, Double lng);
}