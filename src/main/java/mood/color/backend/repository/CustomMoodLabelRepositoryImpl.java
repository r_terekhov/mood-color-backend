/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.repository;

import mood.color.backend.domain.MoodLabel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.time.Instant;
import java.util.List;

import static mood.color.backend.utils.Constants.DISTANCE_IN_METRES_FOR_CREATION_MOOD_LABEL;

public class CustomMoodLabelRepositoryImpl implements CustomMoodLabelRepository {

    @Autowired
    MongoTemplate mongoTemplate;


    @Override
    public List<MoodLabel> getMoodLabelsByPointAndDistance(Double lat, Double lng, Double distanceInMetres) {
        GeoJsonPoint point = new GeoJsonPoint(lng, lat);
        List<MoodLabel> labels =
                mongoTemplate.find(new Query(Criteria.where("location.coordinates").nearSphere(point).maxDistance(distanceInMetres)).limit(1000), MoodLabel.class);
        return labels;
    }

    @Override
    public List<MoodLabel> findMoodLabelsByCreatorIdAndExpireAtBeforeOrderByExpireAtDescAndByPointAndDistance(String creatorId, Instant expireBefore, Double lat, Double lng) {
        GeoJsonPoint point = new GeoJsonPoint(lng, lat);
        Query query = new Query();
        query
                .addCriteria(Criteria.where("creatorId").is(creatorId))
                .addCriteria(Criteria.where("expireAt").lt(expireBefore))
                .addCriteria(Criteria.where("location.coordinates").nearSphere(point).maxDistance(DISTANCE_IN_METRES_FOR_CREATION_MOOD_LABEL))
                .with(new Sort(Sort.Direction.DESC, "expireAt"))
                .limit(1);
        List<MoodLabel> labels =
                mongoTemplate.find(query, MoodLabel.class);
        return labels;
    }
}