/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.repository;

import mood.color.backend.domain.Graph;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomGraphRepository {
    /*https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#reference*/
    List<Graph> findGraphByPointAndDistance(Double lat, Double lng, Double distanceInKilometres);
}