/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.repository;

import mood.color.backend.domain.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public class CustomEventRepositoryImpl implements CustomEventRepository {

    @Autowired
    MongoTemplate mongoTemplate;


    @Override
    public List<Event> getEventsByPointAndDistance(Double lat, Double lng, Double distanceInMetres, Pageable pageable) {
        GeoJsonPoint point = new GeoJsonPoint(lng, lat);
        Query query = new Query(Criteria.where("location.coordinates").nearSphere(point).maxDistance(distanceInMetres));
        query.with(pageable);
        List<Event> events =
                mongoTemplate.find(query, Event.class);
        return events;
    }
}
