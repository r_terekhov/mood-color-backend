/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.repository;

import mood.color.backend.domain.Event;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CustomEventRepository {

    List<Event> getEventsByPointAndDistance(Double lat, Double lng, Double distanceInMetres, Pageable pageable);
}
