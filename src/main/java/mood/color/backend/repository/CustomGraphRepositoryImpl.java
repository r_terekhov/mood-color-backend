/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.repository;

import mood.color.backend.domain.Graph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public class CustomGraphRepositoryImpl implements CustomGraphRepository {

    @Autowired
    MongoTemplate mongoTemplate;


    @Override
    public List<Graph> findGraphByPointAndDistance(Double lat, Double lng, Double distanceInMetres) {
        GeoJsonPoint point = new GeoJsonPoint(lng, lat);
        List<Graph> graphs =
                mongoTemplate.find(new Query(Criteria.where("location.coordinates").nearSphere(point).maxDistance(distanceInMetres)).limit(1000), Graph.class);
        return graphs;
    }
}