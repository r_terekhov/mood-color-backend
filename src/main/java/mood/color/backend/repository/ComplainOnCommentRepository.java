/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.repository;

import mood.color.backend.domain.ComplainOnComment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface ComplainOnCommentRepository extends MongoRepository<ComplainOnComment, String> {

    Optional<ComplainOnComment> findComplainOnCommentByCommentIdAndUserId(String commentId, String userId);

    Set<ComplainOnComment> findAllByUserId(String userId);
}
