/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.controller;

import mood.color.backend.domain.Comment;
import mood.color.backend.dto.CommentResponse;
import mood.color.backend.dto.CreateCommentRequestDto;
import mood.color.backend.model.BaseApiResponse;
import mood.color.backend.service.CommentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.Set;

@RestController
@Validated
@RequestMapping("moodcolor/comment")
public class CommentController {

    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }


    @PostMapping(value = "/create")
    public ResponseEntity<BaseApiResponse<CommentResponse>> createNew(
            @Valid @RequestBody CreateCommentRequestDto createCommentRequestDto) {
        Comment comment = commentService.createComment(
                createCommentRequestDto.getCreatorId(),
                createCommentRequestDto.getAuthorNick(),
                createCommentRequestDto.getAuthorGender(),
                createCommentRequestDto.getMoodLabelId(),
                createCommentRequestDto.getBody());

        CommentResponse commentResponse = new CommentResponse(
                comment.getId(),
                comment.getCreated().toEpochMilli(),
                comment.getCreatorId(),
                comment.getAuthorNick(),
                comment.getAuthorGender(),
                comment.getMoodLabelId(),
                comment.getBody());

        return new ResponseEntity<>(BaseApiResponse.response(commentResponse), HttpStatus.CREATED);
    }


    @PutMapping(value = "/edit")
    public ResponseEntity<BaseApiResponse<CommentResponse>> editComment(
            @RequestParam String id,
            @RequestParam String body) {
        Comment comment = commentService.editComment(id, body);


        CommentResponse commentResponse = new CommentResponse(
                comment.getId(),
                comment.getCreated().toEpochMilli(),
                comment.getCreatorId(),
                comment.getAuthorNick(),
                comment.getAuthorGender(),
                comment.getMoodLabelId(),
                comment.getBody());


        return new ResponseEntity<>(BaseApiResponse.response(commentResponse), HttpStatus.OK);
    }


    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<BaseApiResponse<CommentResponse>> deleteComment(
            @PathVariable String id) {
        commentService.deleteComment(id);
        return new ResponseEntity<>(BaseApiResponse.response(null), HttpStatus.OK);
    }


    @PostMapping(value = "/getCommentsForLabels/{userId}")
    public ResponseEntity<BaseApiResponse<Set<CommentResponse>>> getCommentsForLabels(
            @PathVariable String userId,
            @RequestBody Set<String> labelsIds) {
        Set<CommentResponse> commentsForLabels = commentService.getCommentsForLabels(userId, labelsIds);
        return new ResponseEntity<>(BaseApiResponse.response(commentsForLabels), HttpStatus.OK);
    }

    @PostMapping(value = "/complainOnComment")
    public ResponseEntity<BaseApiResponse<Boolean>> complainOnComment(
            @RequestParam String commentId,
            @RequestParam String userId) {
        commentService.complainOnComment(commentId, userId);
        return new ResponseEntity<>(BaseApiResponse.response(true), HttpStatus.OK);
    }


    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public @ResponseBody
    ResponseEntity<BaseApiResponse> onNotFound(EntityNotFoundException ex) {
        BaseApiResponse response = BaseApiResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseApiResponse> onInvalidRequest(Exception ex) {
        BaseApiResponse response = BaseApiResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
