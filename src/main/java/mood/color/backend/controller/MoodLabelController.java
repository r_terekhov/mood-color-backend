/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.controller;

import mood.color.backend.domain.Graph;
import mood.color.backend.domain.MoodLabel;
import mood.color.backend.dto.CreateLabelRequestDto;
import mood.color.backend.dto.GraphResponse;
import mood.color.backend.dto.LabelResponse;
import mood.color.backend.model.Action;
import mood.color.backend.model.BaseApiResponse;
import mood.color.backend.model.Mood;
import mood.color.backend.model.Status;
import mood.color.backend.service.MoodLabelService;
import mood.color.backend.utils.Utils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@Validated
@RequestMapping("moodcolor/label")
public class MoodLabelController {

    private final MoodLabelService moodLabelService;

    public MoodLabelController(MoodLabelService moodLabelService) {
        this.moodLabelService = moodLabelService;
    }


    @PostMapping(value = "/create")
    public ResponseEntity<BaseApiResponse<LabelResponse>> createNew(
            @Valid @RequestBody CreateLabelRequestDto createLabelRequestDto) {
        MoodLabel newLabel = moodLabelService.createLabel(createLabelRequestDto);

        List<Double> latLng = Utils.convertLocationToLatLng(newLabel.getLocation());

        LabelResponse labelResponse = new LabelResponse(
                newLabel.getId(),
                newLabel.getCreated().toEpochMilli(),
                newLabel.getCreatorId(),
                newLabel.getGenderOfCreator(),
                newLabel.getGraphId(),
                latLng.get(0),
                latLng.get(1),
                Mood.fromMood(newLabel.getMood()),
                Action.fromAction(newLabel.getAction()),
                newLabel.getContents(), newLabel.getLongLive(),
                Status.fromStatus(newLabel.getStatus()), newLabel.getExpireAt().getEpochSecond());

        return new ResponseEntity<>(BaseApiResponse.response(labelResponse), HttpStatus.CREATED);
    }


    @GetMapping(value = "/findAllLabelsByLocationAndDistance")
    public ResponseEntity<BaseApiResponse<List<LabelResponse>>> findAllLabelsByLocationAndDistance(
            @RequestParam Double lat,
            @RequestParam Double lng,
            @RequestParam Double distance) {
        List<MoodLabel> allLabelsByLocationAndDistance = moodLabelService.findAllLabelsByLocationAndDistance(lat, lng, distance);
        List<LabelResponse> labelResponses = new ArrayList<>();

        allLabelsByLocationAndDistance.forEach(moodLabel -> {
            List<Double> latLng = Utils.convertLocationToLatLng(moodLabel.getLocation());

            labelResponses.add(new LabelResponse(
                    moodLabel.getId(),
                    moodLabel.getCreated().toEpochMilli(),
                    moodLabel.getCreatorId(),
                    moodLabel.getGenderOfCreator(),
                    moodLabel.getGraphId(),
                    latLng.get(0),
                    latLng.get(1),
                    Mood.fromMood(moodLabel.getMood()),
                    Action.fromAction(moodLabel.getAction()),
                    moodLabel.getContents(), moodLabel.getLongLive(),
                    Status.fromStatus(moodLabel.getStatus()),
                    moodLabel.getExpireAt().getEpochSecond()));
        });


        return new ResponseEntity<>(BaseApiResponse.response(labelResponses), HttpStatus.OK);
    }


    @GetMapping(value = "/findGraphByLocation")
    public ResponseEntity<BaseApiResponse<GraphResponse>> findGraphByLocation(
            @RequestParam Double lat,
            @RequestParam Double lng) {
        Graph graph = moodLabelService.findGraphByLocation(lat, lng);
        return new ResponseEntity<>(BaseApiResponse.response(GraphResponse.of(graph)), HttpStatus.OK);
    }


    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public @ResponseBody
    ResponseEntity<BaseApiResponse> onNotFound(EntityNotFoundException ex) {
        BaseApiResponse response = BaseApiResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseApiResponse> onInvalidRequest(Exception ex) {
        BaseApiResponse response = BaseApiResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
