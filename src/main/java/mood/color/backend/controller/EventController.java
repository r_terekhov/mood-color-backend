/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.controller;

import mood.color.backend.domain.Event;
import mood.color.backend.dto.ContactResponse;
import mood.color.backend.dto.CreateEventRequestDto;
import mood.color.backend.dto.EventResponse;
import mood.color.backend.model.BaseApiResponse;
import mood.color.backend.model.Contact;
import mood.color.backend.model.ContactType;
import mood.color.backend.service.EventService;
import mood.color.backend.utils.Utils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@Validated
@RequestMapping("moodcolor/event")
public class EventController {

    private final EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @PostMapping(value = "/create")
    public ResponseEntity<BaseApiResponse<EventResponse>> createNew(
            @Valid @RequestBody CreateEventRequestDto createEventRequestDto) {

        Event event = eventService.createEvent(createEventRequestDto);

        List<Double> latLng = Utils.convertLocationToLatLng(event.getLocation());

        List<ContactResponse> contactResponses = null;

        if (event.getContacts() != null) {
            contactResponses = new ArrayList<>();
            for (Contact contact : event.getContacts()) {
                contactResponses.add(new ContactResponse(ContactType.fromContactType(contact.getType()), contact.getValue()));
            }
        }

        EventResponse eventResponse = new EventResponse(
                event.getId(),
                event.getCreated().getEpochSecond(),
                event.getCreatorId(),
                event.getName(),
                event.getPathToAvatar(),
                event.getDescription(),
                event.getDateAndTime().getEpochSecond(),
                event.getHumanReadableGeo(),
                latLng.get(0),
                latLng.get(1),
                contactResponses,
                event.getExpireAt().getEpochSecond(),
                event.getBackgroundColor());

        return new ResponseEntity<>(BaseApiResponse.response(eventResponse), HttpStatus.CREATED);
    }

    @GetMapping(value = "/getEvents")
    public ResponseEntity<BaseApiResponse<List<EventResponse>>> getEvents(
            @RequestParam Double lat,
            @RequestParam Double lng,
            @RequestParam Double distance,
            @RequestParam Integer page) {

        List<EventResponse> eventResponses = new ArrayList<>();
        List<Event> eventsByGeo = eventService.getEventsByGeo(lat, lng, distance, page);


        for (Event event : eventsByGeo) {
            List<Double> latLng = Utils.convertLocationToLatLng(event.getLocation());

            List<ContactResponse> contactResponses = null;

            if (event.getContacts() != null) {
                contactResponses = new ArrayList<>();
                for (Contact contact : event.getContacts()) {
                    contactResponses.add(new ContactResponse(ContactType.fromContactType(contact.getType()), contact.getValue()));
                }
            }

            EventResponse eventResponse = new EventResponse(
                    event.getId(),
                    event.getCreated().getEpochSecond(),
                    event.getCreatorId(),
                    event.getName(),
                    event.getPathToAvatar(),
                    event.getDescription(),
                    event.getDateAndTime().getEpochSecond(),
                    event.getHumanReadableGeo(),
                    latLng.get(0),
                    latLng.get(1),
                    contactResponses,
                    event.getExpireAt().getEpochSecond(),
                    event.getBackgroundColor());
            eventResponses.add(eventResponse);
        }


        return new ResponseEntity<>(BaseApiResponse.response(eventResponses), HttpStatus.OK);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public @ResponseBody
    ResponseEntity<BaseApiResponse> onNotFound(EntityNotFoundException ex) {
        BaseApiResponse response = BaseApiResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseApiResponse> onInvalidRequest(Exception ex) {
        BaseApiResponse response = BaseApiResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
