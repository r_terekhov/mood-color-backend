/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.controller;

import mood.color.backend.domain.User;
import mood.color.backend.dto.UserRegistrationRequestDto;
import mood.color.backend.dto.UserResponse;
import mood.color.backend.model.BaseApiResponse;
import mood.color.backend.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
@Validated
@RequestMapping("moodcolor/user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/signup")
    public ResponseEntity<BaseApiResponse<UserResponse>> register(
            @Valid @RequestBody UserRegistrationRequestDto userRegistrationRequestDto) {
        User registredUser = userService.register(userRegistrationRequestDto);
        UserResponse response = UserResponse.of(registredUser);
        return new ResponseEntity<>(BaseApiResponse.response(response), HttpStatus.CREATED);
    }

    @PutMapping(value = "/updateGeo/{id}")
    public ResponseEntity<BaseApiResponse<UserResponse>> updateUserGeo(
            @Valid @PathVariable String id,
            @Valid @RequestParam Double lat,
            @Valid @RequestParam Double lng) {
        User updatedUser = userService.updateUserGeo(id, lat, lng);
        UserResponse userResponse = UserResponse.of(updatedUser);
        return new ResponseEntity<>(BaseApiResponse.response(userResponse), HttpStatus.OK);
    }

    @PutMapping(value = "/updateGenderAndNick/{id}")
    public ResponseEntity<BaseApiResponse<UserResponse>> updateUserGeo(
            @PathVariable String id,
            @RequestParam Integer gender,
            @RequestParam String nick) {
        User updatedUser = userService.setupGenderAndNick(id, gender, nick);
        UserResponse response = UserResponse.of(updatedUser);
        return new ResponseEntity<>(BaseApiResponse.response(response), HttpStatus.OK);
    }

    @PutMapping(value = "/updatePushToken/{id}")
    public ResponseEntity<BaseApiResponse<UserResponse>> updatePushToken(
            @Valid @PathVariable String id,
            @RequestParam @NotBlank String pushToken) {
        User updatedUser = userService.updatePushToken(id, pushToken);
        UserResponse userResponse = UserResponse.of(updatedUser);
        return new ResponseEntity<>(BaseApiResponse.response(userResponse), HttpStatus.OK);
    }


    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public @ResponseBody
    ResponseEntity<BaseApiResponse> onNotFound(EntityNotFoundException ex) {
        BaseApiResponse response = BaseApiResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseApiResponse> onInvalidRequest(Exception ex) {
        BaseApiResponse response = BaseApiResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
