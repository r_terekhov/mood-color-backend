/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.controller;

import mood.color.backend.properties.Properties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    private final Properties properties;

    public TestController(Properties properties) {
        this.properties = properties;
    }

    @GetMapping(value = "moodcolor/test")
    public ResponseEntity<String> testMethod() {
        return new ResponseEntity<>("App is alive " + properties.getAll(), HttpStatus.OK);
    }
}
