/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.utils;

public class Constants {

    public static final int DISTANCE_IN_METRES_FOR_CREATION_MOOD_LABEL = 100;


    public static final int DEFAULT_VALUE_FOR_GRAPH_EXPIRE_AT_IN_DAYS = 7;
    public static final int DEFAULT_VALUE_FOR_COMMENTS_EXPIRE_AT_IN_HOURS = 5;
    public static final int DEFAULT_VALUE_FOR_LONG_LIVE_MOOD_LABELS_EXPIRE_AT_IN_DAYS = 30;

    public static final double MIN_DISTANCE_IN_METRES = 500;
}
