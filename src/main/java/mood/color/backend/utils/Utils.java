/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.utils;

import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Utils {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

    public static List<Double> convertLocationToLatLng(GeoJsonPoint location) {
        List<Double> locationInLatLng = new ArrayList<>();

        if (location != null) {
            List<Double> loc = location.getCoordinates();
            locationInLatLng.add(loc.get(1));
            locationInLatLng.add(loc.get(0));
        } else {
            locationInLatLng.add(null);
            locationInLatLng.add(null);
        }
        return locationInLatLng;
    }

    public static Long convertDateToEpochSecond(String dateInString) {
        return LocalDateTime.parse(dateInString, formatter)
                .atZone(ZoneId.of("UTC"))
                .toInstant().getEpochSecond();
    }

    public static String covertEpochSecondToString(long epochSecond) {
        return ZonedDateTime.ofInstant(Instant.ofEpochSecond(epochSecond), ZoneId.of("UTC")).format(formatter);
    }
}
