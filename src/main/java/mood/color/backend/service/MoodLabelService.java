/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.service;

import mood.color.backend.domain.Graph;
import mood.color.backend.domain.MoodLabel;
import mood.color.backend.dto.CreateLabelRequestDto;

import java.util.List;

public interface MoodLabelService {

    MoodLabel createLabel(CreateLabelRequestDto createLabelRequestDto);

    /**
     * @param lat
     * @param lng
     * @return ближайший граф к точке
     */
    Graph findGraphByLocation(Double lat, Double lng);

    List<MoodLabel> findAllLabelsByLocationAndDistance(Double lat, Double lng, Double distanceInKilometres);
}
