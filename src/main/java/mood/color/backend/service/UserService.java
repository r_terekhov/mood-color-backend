/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.service;

import mood.color.backend.domain.User;
import mood.color.backend.dto.UserRegistrationRequestDto;

public interface UserService {

    User register(UserRegistrationRequestDto userRegistrationRequestDto);

    User updateUserGeo(String userId, Double lat, Double lng);

    User setupGenderAndNick(String userId, Integer gender, String nick);

    User updatePushToken(String userId, String newPushToken);

}
