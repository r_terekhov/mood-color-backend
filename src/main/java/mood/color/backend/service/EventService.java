/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.service;

import mood.color.backend.domain.Event;
import mood.color.backend.dto.CreateEventRequestDto;

import java.util.List;

public interface EventService {

    Event createEvent(CreateEventRequestDto createEventRequestDto);

    List<Event> getEventsByGeo(double lat, double lng, double distanceInMetres, int page);
}
