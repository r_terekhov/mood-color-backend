/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.service;

import mood.color.backend.domain.Comment;
import mood.color.backend.dto.CommentResponse;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

public interface CommentService {

    Comment createComment(@NotBlank String creatorId, @NotBlank String authorNick, @NotNull Integer authorGender, @NotBlank String moodLabelId, @NotBlank String body);

    Comment createCommentOnCreationLabel(@NotBlank String creatorId, @NotBlank String moodLabelId, @NotBlank String body);

    Boolean complainOnComment(@NotBlank String commentId, @NotBlank String userId);

    void deleteComment(String commentId);

    Comment editComment(String commentId, String newBody);

    Set<CommentResponse> getCommentsForLabels(@NotBlank String userId, Set<String> labelsIds);
}
