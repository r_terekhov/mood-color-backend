/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.service.impl;

import mood.color.backend.domain.User;
import mood.color.backend.dto.UserRegistrationRequestDto;
import mood.color.backend.repository.UserRepository;
import mood.color.backend.service.UserService;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User register(UserRegistrationRequestDto userRegistrationRequestDto) {
        User user = new User(
                userRegistrationRequestDto.getPushToken(),
                userRegistrationRequestDto.getUniqueId(),
                userRegistrationRequestDto.getZoneOffset(),
                userRegistrationRequestDto.getLocale()
        );

        User savedUser = userRepository.save(user);
        savedUser.setNick(setupNick(savedUser.getId(), userRegistrationRequestDto.getLocale()));
        User savedUserAndSetupedNick = userRepository.save(savedUser);
        return savedUserAndSetupedNick;
    }

    @Override
    public User updateUserGeo(String userId, Double lat, Double lng) {
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(String.format("User with id %s not found", userId)));
        user.setLocation(new GeoJsonPoint(lng, lat));
        User updatedUser = userRepository.save(user);
        return updatedUser;
    }

    @Override
    public User setupGenderAndNick(String userId, Integer gender, String nick) {
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(String.format("User with id %s not found", userId)));
        user.setGender(gender);
        user.setNick(nick);
        User updatedUser = userRepository.save(user);
        return user;
    }

    @Override
    public User updatePushToken(String userId, String newPushToken) {
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(String.format("User with id %s not found", userId)));
        user.setPushToken(newPushToken);
        User updatedUser = userRepository.save(user);
        return updatedUser;
    }


    private String setupNick(String userId, String locale) {
        String uniqueId = userId.toString().substring(0, 8);

        switch (locale) {
            case "ru": {
                return "Неопознанный енот #" + uniqueId;
            }

            case "en":

            default: {
                return "Unidentified raccoon #" + uniqueId;
            }

        }
    }
}
