/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.service.impl;

import mood.color.backend.component.UploadFileComponent;
import mood.color.backend.domain.Event;
import mood.color.backend.dto.ContactResponse;
import mood.color.backend.dto.CreateEventRequestDto;
import mood.color.backend.model.Contact;
import mood.color.backend.model.ContactType;
import mood.color.backend.repository.EventRepository;
import mood.color.backend.service.EventService;
import mood.color.backend.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service
public class EventServiceImpl implements EventService {

    private static final int PAGE_SIZE = 100;
    private static final Logger log = LoggerFactory.getLogger(EventServiceImpl.class);
    private final EventRepository eventRepository;
    private final UploadFileComponent uploadFileComponent;

    public EventServiceImpl(EventRepository eventRepository, UploadFileComponent uploadFileComponent) {
        this.eventRepository = eventRepository;
        this.uploadFileComponent = uploadFileComponent;
    }


    @Override
    public Event createEvent(CreateEventRequestDto createEventRequestDto) {
        String pathToAvatar;
        try {
            pathToAvatar = uploadFileComponent.uploadFileToFirebase(createEventRequestDto.getAvatar());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            log.error("error on loading avatar, cause : {}", e.getMessage());
            return null;
        }

        Instant dateAndTime = Instant.ofEpochSecond(Utils.convertDateToEpochSecond(createEventRequestDto.getDateAndTime()));
        Instant expireAt = dateAndTime.plus(2, ChronoUnit.DAYS);

        List<Contact> contacts = null;

        if (createEventRequestDto.getContacts() != null) {
            contacts = new ArrayList<>();
            for (ContactResponse contactResponse : createEventRequestDto.getContacts()) {
                contacts.add(new Contact(ContactType.fromString(contactResponse.getType()), contactResponse.getValue()));
            }
        }

        Event event = new Event(expireAt,
                createEventRequestDto.getCreatorId(),
                createEventRequestDto.getName(),
                pathToAvatar,
                createEventRequestDto.getDescription(),
                dateAndTime,
                createEventRequestDto.getHumanReadableGeo(),
                new GeoJsonPoint(new org.springframework.data.geo.Point(createEventRequestDto.getLng(), createEventRequestDto.getLat())),
                contacts,
                createEventRequestDto.getBackgroundColor()
        );

        return eventRepository.save(event);
    }

    @Override
    public List<Event> getEventsByGeo(double lat, double lng, double distanceInMetres, int page) {
        return eventRepository.getEventsByPointAndDistance(lat, lng, distanceInMetres, PageRequest.of(page, PAGE_SIZE));
    }
}
