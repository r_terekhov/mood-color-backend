/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.service.impl;

import mood.color.backend.domain.Graph;
import mood.color.backend.domain.MoodLabel;
import mood.color.backend.dto.CreateLabelRequestDto;
import mood.color.backend.model.Action;
import mood.color.backend.model.Mood;
import mood.color.backend.model.Status;
import mood.color.backend.properties.Properties;
import mood.color.backend.repository.GraphRepository;
import mood.color.backend.repository.MoodLabelRepository;
import mood.color.backend.service.CommentService;
import mood.color.backend.service.MoodLabelService;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static java.lang.String.format;
import static mood.color.backend.utils.Constants.MIN_DISTANCE_IN_METRES;

@Service
public class MoodLabelServiceImpl implements MoodLabelService {

    private final MoodLabelRepository moodLabelRepository;
    private final GraphRepository graphRepository;
    private final CommentService commentService;
    private final Properties properties;


    public MoodLabelServiceImpl(MoodLabelRepository moodLabelRepository, GraphRepository graphRepository, CommentService commentService, Properties properties) {
        this.moodLabelRepository = moodLabelRepository;
        this.graphRepository = graphRepository;
        this.commentService = commentService;
        this.properties = properties;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public MoodLabel createLabel(CreateLabelRequestDto createLabelRequestDto) {
        Graph graph = findGraphByLocation(createLabelRequestDto.getLat(), createLabelRequestDto.getLng());

        MoodLabel lastCreatedAliveLabelInCertainPlaceForCertainUser = getLastCreatedAliveLabelInCertainPlaceForCertainUser(
                createLabelRequestDto.getCreatorId(),
                createLabelRequestDto.getLat(),
                createLabelRequestDto.getLng());

        if (graph == null) {
            graph = new Graph(
                    createLabelRequestDto.getCreatorId(),
                    new GeoJsonPoint(new org.springframework.data.geo.Point(createLabelRequestDto.getLng(), createLabelRequestDto.getLat())));

            graph = graphRepository.save(graph);
        }

        Instant expireAt;

        if (createLabelRequestDto.getLongLive() == null || !createLabelRequestDto.getLongLive()) {
            expireAt = Instant.now().plus(Integer.parseInt(properties.getAll()), ChronoUnit.HOURS);
        } else {
            expireAt = Instant.now().plus(Integer.parseInt(properties.getAll()), ChronoUnit.HOURS);
        }

        if (lastCreatedAliveLabelInCertainPlaceForCertainUser == null) {
            MoodLabel moodLabel = new MoodLabel(
                    expireAt,
                    createLabelRequestDto.getCreatorId(),
                    createLabelRequestDto.getGenderOfCreator(),
                    graph.getId(),
                    new GeoJsonPoint(new org.springframework.data.geo.Point(createLabelRequestDto.getLng(), createLabelRequestDto.getLat())),
                    Mood.fromInteger(createLabelRequestDto.getMood()),
                    Action.fromInteger(createLabelRequestDto.getAction()),
                    createLabelRequestDto.getLongLive(),
                    Status.ALIVE
            );

            MoodLabel savedLabel = moodLabelRepository.save(moodLabel);
            graph.setAmountOfLabelsInGraph(graph.getAmountOfLabelsInGraph() + 1);
            graphRepository.save(graph);

            if (!StringUtils.isEmpty(createLabelRequestDto.getMessage())) {
                commentService.createCommentOnCreationLabel(createLabelRequestDto.getCreatorId(), savedLabel.getId(), createLabelRequestDto.getMessage());
            }

            return savedLabel;
        } else {
            lastCreatedAliveLabelInCertainPlaceForCertainUser.setLocation(new GeoJsonPoint(new org.springframework.data.geo.Point(createLabelRequestDto.getLng(), createLabelRequestDto.getLat())));
            MoodLabel savedLabel = moodLabelRepository.save(lastCreatedAliveLabelInCertainPlaceForCertainUser);

            if (!StringUtils.isEmpty(createLabelRequestDto.getMessage())) {
                commentService.createCommentOnCreationLabel(createLabelRequestDto.getCreatorId(), savedLabel.getId(), createLabelRequestDto.getMessage());
            }
            return savedLabel;
        }


    }

    @Override
    public Graph findGraphByLocation(Double lat, Double lng) {

        List<Graph> graphByPointAndDistance = graphRepository.findGraphByPointAndDistance(lat, lng, MIN_DISTANCE_IN_METRES);

        if (graphByPointAndDistance.size() == 0) {
            return null;
        } else {
            return graphByPointAndDistance.get(0);
        }

    }

    @Override
    public List<MoodLabel> findAllLabelsByLocationAndDistance(Double lat, Double lng, Double distanceInKilometres) {
        List<MoodLabel> moodLabelsByPointAndDistance = moodLabelRepository.getMoodLabelsByPointAndDistance(lat, lng, distanceInKilometres);


        if (moodLabelsByPointAndDistance.size() == 0) {
            throw new EntityNotFoundException(format("MoodLabel for point with lat: %s lng: %s and distance: %s not found", lat, lng, distanceInKilometres));
        } else {
            return moodLabelsByPointAndDistance;
        }
    }

    private MoodLabel getLastCreatedAliveLabelInCertainPlaceForCertainUser(String userId, Double lat, Double lng) {
        List<MoodLabel> labels = moodLabelRepository
                .findMoodLabelsByCreatorIdAndExpireAtBeforeOrderByExpireAtDescAndByPointAndDistance(
                        userId,
                        Instant.now().plus(Integer.parseInt(properties.getMoodLabelTtl()), ChronoUnit.HOURS),
                        lat,
                        lng
                );

        if (labels == null || labels.size() == 0) {
            return null;
        }

        return labels.get(0);
    }
}
