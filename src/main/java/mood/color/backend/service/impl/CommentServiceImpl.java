/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package mood.color.backend.service.impl;

import mood.color.backend.domain.Comment;
import mood.color.backend.domain.ComplainOnComment;
import mood.color.backend.domain.User;
import mood.color.backend.dto.CommentResponse;
import mood.color.backend.properties.Properties;
import mood.color.backend.repository.CommentRepository;
import mood.color.backend.repository.ComplainOnCommentRepository;
import mood.color.backend.repository.MoodLabelRepository;
import mood.color.backend.repository.UserRepository;
import mood.color.backend.service.CommentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotBlank;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.Set;

@Service
public class CommentServiceImpl implements CommentService {

    private static final Logger log = LoggerFactory.getLogger(CommentServiceImpl.class);
    private static final int PAGE_LIMIT = 100;
    private final CommentRepository commentRepository;
    private final UserRepository userRepository;
    private final MoodLabelRepository moodLabelRepository;
    private final ComplainOnCommentRepository complainOnCommentRepository;
    private final Properties properties;

    public CommentServiceImpl(CommentRepository commentRepository, UserRepository userRepository, MoodLabelRepository moodLabelRepository, ComplainOnCommentRepository complainOnCommentRepository, Properties properties) {
        this.commentRepository = commentRepository;
        this.userRepository = userRepository;
        this.moodLabelRepository = moodLabelRepository;
        this.complainOnCommentRepository = complainOnCommentRepository;
        this.properties = properties;
    }

    @Override
    public Comment createComment(String creatorId, String authorNick, Integer authorGender, String moodLabelId, String body) {
        Comment comment = new Comment(
                Instant.now().plus(Integer.parseInt(properties.getAll()), ChronoUnit.HOURS),
                creatorId,
                authorNick,
                authorGender,
                moodLabelId,
                body);
        final Comment savedComment = commentRepository.save(comment);
        return savedComment;
    }

    @Override
    public Comment createCommentOnCreationLabel(@NotBlank String creatorId, @NotBlank String moodLabelId, @NotBlank String body) {
        User user = userRepository.findById(creatorId).orElseThrow(() -> new EntityNotFoundException(String.format("User with id %s not found", creatorId)));
        moodLabelRepository.findById(moodLabelId).orElseThrow(() -> new EntityNotFoundException(String.format("Label with id %s not found", moodLabelId)));

        Comment comment = new Comment(
                Instant.now().plus(Integer.parseInt(properties.getAll()), ChronoUnit.HOURS),
                creatorId,
                user.getNick(),
                user.getGender(),
                moodLabelId,
                body);
        final Comment savedComment = commentRepository.save(comment);
        return savedComment;

    }


    @Override
    public Boolean complainOnComment(@NotBlank String commentId, @NotBlank String userId) {

        Comment comment = commentRepository.findById(commentId).orElseThrow(() -> new EntityNotFoundException(String.format("Comment with id %s not found", commentId)));
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(String.format("User with id %s not found", userId)));

        complainOnCommentRepository.save(new ComplainOnComment(commentId, userId));

        //todo auto delete comment in another thread
        return true;
    }

    @Override
    public void deleteComment(String commentId) {
        commentRepository.deleteById(commentId);
    }

    @Override
    public Comment editComment(String commentId, String newBody) {
        Comment comment = commentRepository.findById(commentId).orElseThrow(() -> new EntityNotFoundException(String.format("Comment with id %s not found", commentId)));

        comment.setBody(newBody);
        Comment savedComment = commentRepository.save(comment);
        return savedComment;
    }

    @Override
    public Set<CommentResponse> getCommentsForLabels(String userId, Set<String> labelsIds) {

        Set<CommentResponse> comments = new HashSet<>();

        //todo pageable
        Set<ComplainOnComment> excludedCommentsForUser = complainOnCommentRepository.findAllByUserId(userId);

        Set<String> excludedCommentsForUserIds = new HashSet<>();

        for (ComplainOnComment complainOnComment : excludedCommentsForUser) {
            excludedCommentsForUserIds.add(complainOnComment.getCommentId());
        }

        Page<Comment> commentsPage;
        int pageNumber = 0;
        do {
            commentsPage = commentRepository.findAllByMoodLabelIdInAndIdNotIn(labelsIds, excludedCommentsForUserIds, PageRequest.of(pageNumber, PAGE_LIMIT, Sort.by("created").ascending()));
            log.debug("comments for labels {}/{}", pageNumber, commentsPage.getTotalPages());

            commentsPage.get().forEach(comment -> {
                CommentResponse commentResponse = new CommentResponse(
                        comment.getId(),
                        comment.getCreated().toEpochMilli(),
                        comment.getCreatorId(),
                        comment.getAuthorNick(),
                        comment.getAuthorGender(),
                        comment.getMoodLabelId(),
                        comment.getBody());


                comments.add(commentResponse);
            });
            pageNumber++;
        } while (!commentsPage.isLast());


        if (comments.size() == 0) {
            throw new EntityNotFoundException("No comments found for moodLabelsIds " + labelsIds);
        }

        return comments;

    }
}
